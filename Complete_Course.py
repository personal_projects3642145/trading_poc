# This course was made in Colaborative Laboratory over the Google Drive colab.research.google.com
# I re-written everything here... so is not tested in PyCharm... but this works in colab.research.google.com,
# I can assure that


# Block 1 ==> First api call as an example
""" Important page that we will use for this https://iexcloud.io/marketplace/
import requests

symbol = "GOOG"
IEX_CLOUD_API_TOKEN = "Tpk_303eba2884a34a0e937ec42ab848532a"

api_url = f"https://sandbox.iexapis.com/stable/stock/{symbol}/quote?token={IEX_CLOUD_API_TOKEN}"

data = requests.get(api_url).json()

print(data)
"""

# Block 2 ==> Get the list of the S&P500 companies
"""import pandas

page = pandas.read_html("https://en.wikipedia.org/wiki/List_of_S%26P_500_companies")

stocks_table = page[0]

stocks_table.info()
"""

# Block 3 ==> Get the ticker, price, market capitalization and shares to buy of all the companies in the S&P500
"""stocks_columns = ["Ticker", "Price", "Market Capitalization", "Shares to Buy"]
stocks_dataframe = pandas.DataFrame(columns=stocks_columns)

for symbol in stocks_table["Symbol"]:
    api_url = f"https://sandbox.iexapis.com/stable/stock/{symbol}/quote?token={IEX_CLOUD_API_TOKEN}"

    data = requests.get(api_url).json()

    stocks_dataframe = stocks_dataframe.append(pandas.Series([symbol,
                                                              data["latestPrice"],
                                                              data["marketCap"],
                                                              "N/A"],
                                                             index=stocks_columns),
                                               ignore_index=True)

print(stocks_dataframe)"""

# Block 4 ==> Divide the list of the S&P500 in block 3 to 5 lists of 100
"""def split_list(list, number_per_group):
  for index in range(0, len(list), number_per_group):
    yield list[index:index + number_per_group]

stock_symbols_groups = list(split_list(stocks_table["Symbol"], 100))

print(stock_symbols_groups)
"""

# Block 5 ==> Get all the symbols of the S&P500
"""stock_symbols_strings = []

for index in range(0, len(stock_symbols_groups)):
  stock_symbols_strings.append(",".join(stock_symbols_groups[index]))

  print(stock_symbols_strings[index])
"""

# Block 6 ==> Same than before, but with a faster performance BATCH API CALL
"""batch_stocks_dataframe = pandas.DataFrame(columns=stocks_columns)

for symbol_string in stock_symbols_strings:
    batch_api_call_url = f"https://sandbox.iexapis.com/stable/stock/market/batch/?types=quote&symbols={symbol_string}&token={IEX_CLOUD_API_TOKEN}"
    data = requests.get(batch_api_call_url).json()

    for symbol in symbol_string.split(","):
        batch_stocks_dataframe = batch_stocks_dataframe.append(pandas.Series([symbol,
                                                                              data[symbol]["quote"]["latestPrice"],
                                                                              data[symbol]["quote"]["marketCap"],
                                                                              "N/A"],
                                                                             index=stocks_columns),
                                                               ignore_index=True)

print(batch_stocks_dataframe)
"""

# Block 7 ==> Input quantity of money and identify the quantity of stocks possible to buy for each company
"""import math

portfolio_value = input("Enter your cash. This will indicate quantity of stocks possible to buy: ")

try:
    portfolio_value_float = float(portfolio_value)
except ValueError:
    print("Error in the input value")

position_size = float(portfolio_value) / len(batch_stocks_dataframe.index)
print(position_size)
print(batch_stocks_dataframe.index)
print(len(batch_stocks_dataframe.index))

for index in range(0, len(batch_stocks_dataframe["Ticker"]) - 1):
    batch_stocks_dataframe.loc[index, "Shares to Buy"] = math.floor(
        position_size / batch_stocks_dataframe["Price"][index])

batch_stocks_dataframe"""

# Block 8 ==> Export to an excel file
"""!pip install xlsxwriter

import xlsxwriter

excel_writer = pandas.ExcelWriter("stocks.xlsx", engine="xlsxwriter")

batch_stocks_dataframe.to_excel(excel_writer,
                                sheet_name="S&P 500 Stocks",
                                index = False)

excel_writer.save()
"""
